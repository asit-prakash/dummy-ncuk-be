const express = require('express');
const router = express.Router();

router.get(
  '/api/student-management/v1/masters/getAllUniversity',
  (req, res) => {
    const data = [
      {
        region_id: 1,
        region_name: 'New Delhi',
      },
      {
        region_id: 2,
        region_name: 'Bangalore',
      },
      {
        region_id: 3,
        region_name: 'Mumbai',
      },
    ];
    res.status(200).json({
      code: 200,
      message: 'Success',
      description: 'University List',
      data,
    });
    // res.status(400).json({ message: 'Bad Request' });
    // res.status(404).json({ message: 'Not found' });
    // res.status(500).json({ message: 'Internal server error' });
  }
);

module.exports = router;
